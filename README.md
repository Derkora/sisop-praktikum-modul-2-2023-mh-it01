# sisop-praktikum-modul-2-2023-MH-IT01

## Anggota Kelompok

| NRP        | Nama                    |
|:----------:|:-----------------------:|
| 5027221021 | Steven Figo             |
| 5027221024 | Wilson Matthew Thendry  |
| 5027221048 | Muhammad Arsy Athallah  |

- [Peraturan](#peraturan) 
- [Soal](#soal)
- [Penjelasan](#penjelasan)
  - [Soal 1](#soal-1)
  - [Soal 2](#soal-2)
  - [Soal 3](#soal-3)
  - [Soal 4](#soal-4)
- [Revisi](#revisi)

## Peraturan

1. Waktu pengerjaan dimulai Senin (2/10) setelah sesi lab hingga Sabtu (7/10) pukul 22.00 WIB.
2. Praktikan diharapkan membuat laporan penjelasan dan penyelesaian soal dalam bentuk Readme(gitlab).
3. Format nama repository gitlab “sisop-praktikum-modul-[Nomor Modul]-2023-[Kode Dosen Kelas]-[Nama Kelompok]” (contoh:sisop-praktikum-modul-2-2023-MH-IT01).
Struktur repository seperti berikut:
 - soal_1:
   - cleaner.c
 - soal_2:
   - cavs.c
 - soal_3:
   - lukisan.c
 - soal_4:
   - antivirus.c
4. Jika **melanggar struktur repo** akan **dianggap sama dengan curang** dan menerima konsekuensi **sama** dengan melakukan **kecurangan**.
5. Setelah pengerjaan selesai, semua script bash, awk, dan file yang berisi cron job ditaruh di gitlab masing - masing kelompok, dan link gitlab diletakkan pada form yang disediakan. Pastikan gitlab di setting ke publik.
6. Commit terakhir maksimal 10 menit setelah waktu pengerjaan berakhir. Jika melewati maka akan dinilai berdasarkan commit terakhir.
7. Jika tidak ada pengumuman perubahan soal oleh asisten, maka soal dianggap dapat diselesaikan.
8. Jika ditemukan soal yang tidak dapat diselesaikan, harap menuliskannya pada Readme beserta permasalahan yang ditemukan.
9. Praktikan tidak diperbolehkan menanyakan jawaban dari soal yang diberikan kepada asisten maupun praktikan dari kelompok lainnya.
10. Jika ditemukan **indikasi kecurangan** dalam bentuk apapun di pengerjaan soal shift, maka **nilai dianggap 0**.
11. Pengerjaan soal shift sesuai dengan modul yang telah diajarkan.
12. Zip dari repository dikirim ke email asisten penguji dengan subjek yang sama dengan nama judul repository, dikirim sebelum deadline dari soal shift
13. Jika terdapat revisi soal akan dituliskan pada halaman terakhir

## soal

1. John adalah seorang mahasiswa biasa. Pada tahun ke-dua kuliahnya, dia merasa bahwa dia telah menyianyiakan waktu kuliahnya selama ini. Selama mengerjakan tugas, John selalu menggunakan bantuan ChatGPT dan tidak pernah mempelajari apapun dari hal tersebut. Untuk itu, pada soal kali ini John bertekad untuk tidak menggunakan ChatGPT dan mencoba menyelesaikan tugasnya dengan tangan dan pikirannya sendiri. Melihat tekad yang kuat dari John, Mark, dosen yang mengajar John, ingin membantunya belajar dengan memberikan sebuah ujian. Sebelum memberikan ujian pada John, Mark berpesan bahwa John harus bersungguh-sungguh dalam mengerjakan ujian, fokus untuk belajar, dan tidak perlu khawatir akan nilai yang diberikan. Mark memberikan ujian pada John untuk membuatkannya sebuah program `cleaner` sederhana dengan ketentuan berikut :
- Program tersebut menerima input path dengan menggunakan “**argv**”.
- Program tersebut bertugas untuk menghapus file yang didalamnya terdapat string "SUSPICIOUS" pada direktori yang telah diinputkan oleh user.
- Program tersebut harus berjalan secara daemon.
- Program tersebut akan terus berjalan di background dengan jeda **30 detik**.
- Dalam pembuatan program tersebut, tidak diperbolehkan menggunakan fungsi `system()`.
- Setiap kali program tersebut menghapus sebuah file, maka akan dicatat pada file 'cleaner.log' yang ada pada direktori home user dengan format seperti berikut : “**[YYYY-mm-dd HH:MM:SS] '<absolute_path_to_file>' has been removed.**”.

- Example usage:	
```bash
./cleaner '/home/user/cleaner'
```

- Example cleaner.log
```txt
[2023-10-02 18:54:20] '/home/user/cleaner/is.txt' has been removed.
[2023-10-02 18:54:20] '/home/user/cleaner/that.txt' has been removed.
[2023-10-02 18:54:50] '/home/user/cleaner/who.txt' has been removed.
```

- Example valid sample file:
```txt
LJuUHmAnVLLCMRhLTcqy
bBpSUSPICIOUSagQKmLA
BitSuQNHSLmZDvEcvbGc
```

- Example invalid sample file:
```txt
LJuUHmAnVLLCMRhLTcqy
TFvefehhpWDCbkdirmlh
BitSuQNHSLmZDvEcvbGc
```

2. QQ adalah fan Cleveland Cavaliers. Ia ingin memajang kamarnya dengan poster [foto roster Cleveland Cavaliers tahun 2016](https://en.wikipedia.org/wiki/2015%E2%80%9316_Cleveland_Cavaliers_season#Roster). Maka yang dia lakukan adalah meminta tolong temannya yang sangat sisopholic untuk membuatkannya sebuah program untuk mendownload gambar - gambar pemain tersebut. Sebagai teman baiknya, bantu QQ untuk mencarikan foto foto yang dibutuhkan QQ dengan ketentuan sebagai berikut:
- Pertama, buatlah program bernama “**cavs.c**” yang dimana program tersebut akan membuat folder “**players**”.
- Program akan **mengunduh** file yang berisikan [database para pemain bola](https://drive.google.com/file/d/1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK/view?usp=drive_link). Kemudian dalam program yang sama diminta dapat melakukan **extract** “**players.zip**” ke dalam folder **players** yang telah dibuat. Lalu **hapus** file zip tersebut agar tidak memenuhi komputer QQ.
- Dikarenakan database yang diunduh masih data mentah. Maka bantulah QQ untuk menghapus semua pemain yang **bukan** dari Cleveland Cavaliers yang ada di directory.
- Setelah mengetahui nama-nama pemain Cleveland Cavaliers, QQ perlu untuk mengkategorikan pemain tersebut sesuai dengan **posisi** mereka dalam **waktu bersamaan dengan 5 proses yang berbeda**. Untuk kategori folder akan menjadi 5 yaitu **point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C)**.
- Hasil kategorisasi akan di outputkan ke file **Formasi.txt, dengan berisi**
  - **PG: {jumlah pemain}**
  - **SG: {jumlah pemain}**
  - **SF: {jumlah pemain}**
  - **PF: {jumlah pemain}**
  - **C: {jumlah pemain}**
- Ia ingin memajang foto [pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7](https://en.wikipedia.org/wiki/Kyrie_Irving), dengan membuat folder “**clutch**”, yang di dalamnya berisi foto pemain yang bersangkutan.
- Ia merasa kurang lengkap jika tidak memajang **foto pemain yang melakukan [The Block](https://en.wikipedia.org/wiki/The_Block_(basketball)) pada ajang yang sama**, Maka dari itu ditaruhlah foto pemain tersebut di folder “**clutch**” yang sama.
- Catatan:
  - Format nama file yang akan diunduh dalam zip berupa [tim]-[posisi]-[nama].png
  - Tidak boleh menggunakan system(), Gunakan exec() dan fork().
  - Directory “.” dan “..” tidak termasuk yang akan dihapus.
  - 2 poin soal terakhir dilakukan setelah proses kategorisasi selesai

3. Albedo adalah seorang seniman terkenal dari Mondstadt. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini Albedo sedang menghadapi creativity block. Sebagai teman berkebangsaan dari Fontaine yang jago sisop, bantu Albedo untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !
- Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp **[YYYY-MM-dd_HH:mm:ss]**.
- Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://source.unsplash.com/{widthxheight} , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran **(t%1000)+50** piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp **[YYYY-mm-dd_HH:mm:ss]**.
- Agar rapi, setelah sebuah folder telah terisi oleh 10 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip, format nama **[YYYY-mm-dd_HH:mm:ss].zip** tanpa “[]”).
- Karena takut program tersebut lepas kendali, Albedo ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
- Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).
- Catatan :
  - Tidak boleh menggunakan system()
  - Proses berjalan secara daemon
  - Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping

4. Choco adalah seorang ahli pertahanan siber yang tidak suka memakai ChatGPT dalam menyelesaikan masalah. Dia selalu siap melindungi data dan informasi dari ancaman dunia maya. Namun, kali ini, dia membutuhkan bantuan Anda untuk meningkatkan kinerja antivirus yang telah dia buat sebelumnya.
- Bantu Choco dalam mengoptimalkan program antivirus bernama antivirus.c. Program ini seharusnya dapat memeriksa file di folder sisop_infected, dan jika file tersebut diidentifikasi sebagai virus berdasarkan ekstensinya, program harus memindahkannya ke folder quarantine. list dari format ekstensi/tipe file nya bisa didownload di [Link Ini](https://drive.google.com/file/d/1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5/view?usp=sharing) , proses mendownload tidak boleh menggunakan system()
- Daftar ekstensi file yang dianggap virus tersimpan dalam file extensions.csv.
- Ada kejutan di dalam file extensions.csv. Hanya 8 baris pertama yang tidak dienkripsi. Baris-baris setelahnya perlu Anda dekripsi menggunakan algoritma rot13 untuk mengetahui ekstensi virus lainnya.Setiap kali program mendeteksi file virus, catatlah informasi tersebut di virus.log. Format log harus sesuai dengan:
**[nama_user][Dd-Mm-Yy:Hh-Mm-Ss] - {nama file yang terinfeksi} - {tindakan yang diambil}**
  - Contoh:  [sisopUser][29-09-23:08-59-01] - test.locked - Moved to quarantine
  - *nama_user: adalah username dari user yang menambahkan file ter-infected
- Dunia siber tidak pernah tidur, dan demikian juga virus. Choco memerlukan antivirus yang terus berjalan di latar belakang tanpa harus dia intervensi. Dengan menjalankan program ini sebagai Latar belakang, program akan secara otomatis memeriksa folder sisop_infected **setiap detik.
- Choco juga membutuhkan level-level keamanan antivrus jadi dia membuat 3 level yaitu low, medium ,hard. Argumen tersebut di pakai saat menjalankan antivirus.
```
    Low: Hanya me-log file yg terdeteksi
    Medium: log dan memindahkan file yang terdeteksi
    Hard: log dan menghapus file yang terdeteksi
```
ex: **./antivirus -p low**
- Kadang-kadang, Choco mungkin perlu mengganti level keamanan dari antivirus tanpa harus menghentikannya. Integrasikan kemampuan untuk mengganti level keamanan antivirus dengan mengirim sinyal ke daemon. Misalnya, menggunakan SIGUSR1 untuk mode "low", SIGUSR2 untuk "medium", dan SIGRTMIN untuk mode "hard".

Contoh:
```bash
kill -SIGUSR1 <pid_program>
```
- Meskipun penting untuk menjalankan antivirus, ada saat-saat Choco mungkin perlu menonaktifkannya sementara. Bantu dia dengan menyediakan fitur untuk mematikan antivirus dengan cara yang aman dan efisien.

# Penjelasan
## Soal 1
a. Program tersebut menerima input path dengan menggunakan “**argv**”.
```c
cleanerdotC(argv[1]);
```
dengan menggunakan ini untuk inputnya dan untuk isi program dari cleanerdotC itu sendiri
```c
void cleanerdotC(const char *kecleaner) {
    struct dirent *entry;
    struct stat stat;

    DIR *clean = opendir(kecleaner);
    if (!clean) return;

    chdir(kecleaner);

    while((entry = readdir(clean))){
        if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;

        if(lstat(entry->d_name, &stat) == -1) continue;

        if(S_ISDIR(stat.st_mode)) cleanerdotC(entry->d_name);
        else if(S_ISREG(stat.st_mode) && MR_SUS(entry->d_name)){
            remove(entry->d_name);
            buatdotlog(kecleaner, entry->d_name);
        }
        sleep(30);
    }

    closedir(clean);
}
```
program ini pertama akan memeriksa path yang diinput melalui bash jika udah sesuai maka akan masuk kedalam direktori

b. program bertugas untuk mencari dan menghapus
```c
int MR_SUS(const char *filedottxt){
    FILE *fileeee = fopen(filedottxt, "r");
    if(!fileeee) return 0;

    char barise[200];
    while(fgets(barise, sizeof(barise), fileeee))
        if(strstr(barise, "SUSPICIOUS")){
            fclose(fileeee);
            return 1;
        }
    
    fclose(fileeee);
    return 0;
}
```
ini merupakan program yang akan mencari `SUSPICIOUS` dengan memeriksa isi file yang ada didalam directory setelah diperiksa di function `cleanerdotC`  untuk menghapusnya sendiri ada di fucntion `cleanerdotC`
```c
remove(entry->d_name);
```
c. harus berjalan menggunakan daemon sesuai dengan modul yang diberikan aslab maka menggunakan program daemon seperti ini
```c
int main(int argc, char *argv[]){
    pid_t pid, sid;
    pid = fork();
    if(pid < 0){
        exit(EXIT_FAILURE);
    }

    if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    umask(0);
    sid = setsid();
    if(sid < 0){
        exit(EXIT_FAILURE);
    }

    if(chdir("/") < 0){
        exit(EXIT_FAILURE);
    } 

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while(1){
        cleanerdotC(argv[1]);
    }

    return 0;
}
```
d. untuk membuat programnya berjalan setiap 30 detik dengan menggunakan sleep 
```c
sleep(30);
```
e. dari program yang dibuat tidak menggunakan `system()` jadi aman

f. terakhir harus ada `.log`nya dibuat dengan program ini
```c
void buatdotlog(const char *kecleaner, const char *file){
    time_t waktu = time(NULL);
    struct tm *date = localtime(&waktu);
    char stamplog[30];
    strftime(stamplog, 30, "%Y-%m-%d %H:%M:%S", date);

    FILE *lognya = fopen("/home/steven/cleaner.log", "a");
    if(lognya){
        fprintf(lognya, "[%s] '/%s/%s' has been removed.\n", stamplog, kecleaner, file);
        fclose(lognya);
    }
}
```
cara kerja program ini mengambil dari path dan juga file yang lagi diperiksa untuk, serta program ini juga menggunakan waktu untuk menandai kapan `.txt`nya dihapus

sebelum program dijalankan

![image](/uploads/cee93a24355111f41e7e0c614fc93f62/image.png)

setelah dijalankan

![image](/uploads/f3d2cba154ba3d5e19b899ae514d9482/image.png)

hasil dari cleaner.log

![image](/uploads/a1eab77453d98cb37393e2503a546644/image.png)

## Soal 2
a. kita diminta untuk program bernama "**cavs.c**" serta program ini memiliki kemampuan untuk membuat folder "**/players**"
```c
    make = fork();
    if(make == 0){
        char *bashdotsh[] = {"mkdir", "players", NULL};
        execv("/bin/mkdir", bashdotsh);
    }
```
program ini seperti kita melakukan `mkdir players` tapi dengan menggunakan `fork()`` dan execv`()`

b. program diminta untuk download unzip serta hapus itu sama seperti a menggunakan perintah bash seperti wget, unzip, dan rm, gunanya waipid adalah untuk mengunggu prosesnya selesai baru jalan keprogam berikutnya
```c
ngunduh = fork();
    if(ngunduh == 0){
        char *bashdotsh[] = {"wget", "--no-check-certificate", "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "-O", "pemainbb", NULL};
        execv("/usr/bin/wget", bashdotsh);
    }
    waitpid(ngunduh, &status, 0);

    nuzip = fork();
    if(nuzip == 0){
        char *bashdotsh[] = {"unzip", "pemainbb", "-d", "players", NULL};
        execv("/usr/bin/unzip", bashdotsh);
    }
    waitpid(nuzip, &status, 0);

    mr_zip = fork();
    if(mr_zip == 0){
        char *bashdotsh[] = {"rm", "pemainbb", NULL};
        execv("/bin/rm", bashdotsh);
    }
```
c. program diminta untuk menghilangkan pemain selain cavs, dengan menggunakan perintah jika nama `.png` tidak sama dengan `Cavaliers` maka akan dihapus didalam folder `/players`
```c
mr_non_cavs = fork();
    if(mr_non_cavs == 0){
        char *bashdotsh[] = {"find", "players", "-type", "f", "!", "-name", "*Cavaliers*.png", "-delete", NULL};
        execv("/bin/find", bashdotsh);
    }

```
d. membuat folder dan membagi players berdasarkan folder,sama seperti tadi menggunakan mkdir berdanya kita tidak cd ke dir itu tapi kita membuatnya dengan bantuan `-p` dan `players/posisi`. Untuk mengkategorikan dengan menggunakan jika `-posisi-` sams maka dimove dari folder players ke `players/posisi`nya dan kenapa menggunakan `-posisi-` karena kalau tidak maka ada posisi `C` posisi ini akan memindahkan semua yang ada c huruf besar karena nama club ini ada c huruf besar maka ditambah pembatasnya biar ga kepindah semua ketika ketemu `C`
```c
    buat_dirPG = fork();
    if (buat_dirPG == 0){
        char *bashdotsh[] = {"mkdir", "-p", "players/point_guard", NULL};
        execv("/bin/mkdir", bashdotsh);
    }
    //...
    //berikutnya sama cuma beda nama folder doang

    mindah_dirPG = fork();
    if(mindah_dirPG == 0){
        char *bashdotsh[] = {"find", "players", "-type", "f", "-iname", "*-PG-*", "-exec", "mv", "{}", "players/point_guard", ";", NULL};
        execv("/bin/find", bashdotsh);
    }
    while(wait(&status) > 0);
    //...
    //berikutnya sama cuma beda nama folder doang
```
e. ini gunanya untuk memeriksa dari tiap dir posisi dan melakukan penjumlahan, jika ditemukan posisi pada folder itu maka count[i]++
```c
char *folder[] = {"point_guard", "shooting_guard", "small_forward", "power_forward", "center"};
    char shorten[][10] = {"PG", "SG", "SF", "PF", "C"};
    int count[5] = {0};

    for(int i = 0; i < 5; i++){
        char path[100];
        sprintf(path, "players/%s", folder[i]);
        struct dirent *entry;
        DIR *players = opendir(path);
        if(players != NULL){
            while((entry = readdir(players)) != NULL){
                if(entry->d_type == DT_REG) count[i]++;
            }
            closedir(players);
        }
    }
```
sedangkan yang ini menulis nilai penjumlahan yang dilakukan program berikutnya kedalam `Formasi.txt` dengan format
```txt
posisi: jumlahnya
```

```c
  FILE *formasi = fopen("Formasi.txt", "w"); //buat baru
    if(formasi != NULL){
        for (int i = 0; i < 5; i++){
            fprintf(formasi, "%s: %d\n", shorten[i], count[i]);
        }
        fclose(formasi);
    }   
```
f & g. ini digabungin aja karena cuma melakukan copy ke folder cluth dari folder players, berdasarkan informasi yang diberikan maka program seharusnya melakukan copy terhadap `Kyrie` dan `LeBron`
```c
 cp_pemain = fork();
    if(cp_pemain == 0){
        char *bashdotsh[] = {"find", "players", "-type", "f", "-iname", "*Kyrie*", "-exec", "cp", "{}", "clutch", ";", NULL};
        execv("/bin/find", bashdotsh);
    }
    while(wait(&status) > 0 );

    cp_theblock = fork();
    if(cp_theblock == 0){
        char *bashdotsh[] = {"find", "players", "-type", "f", "-iname", "*LeBron*", "-exec", "cp", "{}", "clutch", ";", NULL};
        execv("/bin/find", bashdotsh);
    }
    while(wait(&status) > 0 );
```

hasil dari terminal

![image](/uploads/b68890fefa4b6e31edbdb81671459806/image.png)

isi folder
![image](/uploads/b5d4534b4018cc5dbdb8d39205c52234/image.png)

isi dari formasi.txt
![image](/uploads/9096d19e59fbfa3fdbf04d7a06ddcdf4/image.png)

### `Dokumentasi Error`
- error pada download dan unzip

awalnya karena terjadi error aslab mengira bahwa itu karena dari fork, jadi karena fork membuatnya jadi 2 kali aslab suruh coba menggunakan function

![image](/uploads/b82c3fbcd836e5c1f707e410d653d2ad/image.png)

add return juga masih gagal

![image](/uploads/271e9fd48ea384d4451facd3107d2b18/image.png)

![image](/uploads/b446dbe3fdc67f519536d790e8702771/image.png)

add exit juga

![image](/uploads/819d7c610e1ad2b472d5f1b7a244ce0d/image.png)
kemudian errornya dapat diatasi dengan perintah tambahan ini pada download, gunanya untuk menunggu proses ngunduh
```c
waitpid(ngunduh, &status, 0);
```
dan untuk error unzip sendiri karena dari wslku belum ada unzip jadi harus `apt install unzip` baru programku bekerja sampai bagian b

## Soal 3
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <signal.h>

//albudi a.k.a. calx si sepuh pelukis whwwwwwehwehwhehhwewh

int main(int argc, char** argv) {
    if (argc == 2) {
        FILE* kill_program = fopen("kill.sh", "w");
        fprintf(kill_program, "#!/bin/bash\n");
        if (!strcmp("-a", argv[1])) {
            fprintf(kill_program, "killall -9 lukisan\nrm $0\n");
        } else if (!strcmp("-b", argv[1])) {
            fprintf(kill_program, "kill_parent()\n");
                fprintf(kill_program, "kill ${@: -1}\n");
                fprintf(kill_program, "}\n");
                fprintf(kill_program, "kill_parent $(pidof lukisan)\nrm $0\n");
        }

        fclose(kill_program);

        pid_t chi = fork();
        if (chi < 0){
            exit(EXIT_FAILURE);
        }
        if (chi == 0){
            execlp("bin/chmod", "chmod", "+x", "kill.sh", NULL);
            exit(0);
        }

        pid_t par, sid;

        par = fork();

        if (par < 0) {
            exit(EXIT_FAILURE);
        }
        
        if (par > 0) {
            exit(EXIT_SUCCESS);
        }

        umask(0);

        sid = setsid();
        if (sid < 0) {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
    }

    while (1) {
        pid_t make_folder;
        time_t current = time(NULL);
        time(&current);
        struct tm* time_info;
        time_info = localtime(&current);
        char timeformat[60];
        strftime(timeformat, sizeof(timeformat), "%Y-%m-%d_%H:%M:%S", time_info);
        //printf("%s", timeformat);

        make_folder = fork();

        if (make_folder == 0) {
            execlp("/bin/mkdir", "mkdir", timeformat, NULL);
            exit(0);
        } else {
            pid_t loop_download = fork();
            if (loop_download == 0) {
                for (int i = 0; i < 15; i++) {
                    //donlod gambarnya
                    pid_t download_pic = fork();

                    if (download_pic == 0) {
                        current = time(NULL);
                        char link[100];
                        sprintf(link, "https://source.unsplash.com/%ldx%ld", (current%1000)+50, (current%1000)+50);
                        //printf("%s", link);

                        time_t time_pic;
                        struct tm *time_now_pic;
                        char timeformat_pic[30];
                        char download_dir[100];

                        time(&time_pic);
                        time_now_pic = localtime(&time_pic);
                        strftime(timeformat_pic, sizeof(timeformat_pic), "%Y-%m-%d_%H:%M:%S", time_now_pic);
                        sprintf(download_dir, "%s/%s.jpeg", timeformat, timeformat_pic);

                        char *arg[] = {"wget", "-q", "-O", download_dir, link, NULL};
                        execv("/bin/wget", arg);
                    }
                    sleep(5);
                }
                int status;
                wait(&status);
                char zname[100];
                sprintf(zname, "%s.zip", timeformat);
                pid_t zip_program = fork();

                if (zip_program == 0) {
                    char *arg[] = {"zip", zname, "-rm", timeformat, NULL};
                    execv("/bin/zip", arg);
                }
                exit(0);
            }
        }
        sleep(30);
    }
}

```

Diminta untuk membuat folder setiap 30 detik dan diberi nama dengan waktu pembuatan folder dengan format penamaan “YYYY-mm-dd_HH:MM:SS”

![image](/uploads/82af48a55223c95832a4d1592a08ff89/image.png)

Di dalam setiap folder diminta untuk dilakukan proses download 15 gambar dengan jeda 5 detik maka digunakan proses anak untuk bisa menjalankan proses tersebut  secara independen. Tiap gambar diberi nama sesuai dengan waktu gambar tersebut di-download (format penamaan sama dengan folder yaitu “YYYY-mm-dd_HH:MM:SS”).

![image](/uploads/0166856fa640c663e780a5b260238e34/image.png)

Setelah satu folder terisi penuh 15 gambar maka akan dilakukan zip pada folder tersebut dan mendelete foldernya. Maka digunakan 
```c 
wait(&status)
``` 
untuk menunggu proses download 15 gambar selesai baru menjalankan proses zip.

![image](/uploads/606368ea67e6ffd02afa976b6e12ae2b/image.png)

Terakhir diminta untuk membuat program killer untuk menghentikan proses-proses yang sedang berjalan. Dengan menerapkan mode dalam me-run program utama yaiu “MODE A” dengan argumen “-a” dan “MODE B” dengan argumen “-b”.
- MODE A : Jika file killer dijalankan maka semua proses yang berasal dari program akan dihentikan saat itu juga.
- MODE B : Jika file killer dijalankan maka proses pembuatan folder tiap 30 detik akan dihentikan, namun folder yang sudah dibuat akan tetap diisi sampai 15 gambar dan dilakukan zip.

![image](/uploads/b804a6c3bb3f6a819f4ff0f9ce1b15b3/image.png)

## Soal 4
```c
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/inotify.h>
#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>


#define MAX_LINE_LENGTH 1024
#define EVENT_SIZE (sizeof(struct inotify_event))
#define EVENT_BUF_LEN (1024 * (EVENT_SIZE + 16))


volatile sig_atomic_t running = 1;
volatile int security_level = 0; // Low level security


char rot13(char c) {
    if (isalpha(c)) {
        char base = isupper(c) ? 'A' : 'a';
        return (c - base + 13) % 26 + base;
    }
    return c;
}
```
```<stdio.h>``` : Untuk operasi I/O standar.
```<sys/types.h>``` : Untuk tipe data sistem.
```<sys/wait.h>``` : Untuk fungsi-fungsi terkait proses dan tunggu.
```<unistd.h>```: Untuk sistem panggilan seperti fork dan execlp.
```<string.h>```: Untuk fungsi-fungsi pemrosesan string.
```<ctype.h>```: Untuk fungsi-fungsi pemrosesan karakter.
```<stdlib.h>```: Untuk alokasi memori dinamis.
```<sys/inotify.h>```: Untuk pemantauan perubahan berkas/direktori.
```<signal.h>```: Untuk menangani sinyal.
```<sys/stat.h>```: Untuk informasi status berkas.
```<fcntl.h>```: Untuk operasi pada file descriptor.
```<time.h>```: Untuk pemrosesan waktu.

```MAX_LINE_LENGTH``` : Mendefinisikan panjang maksimum garis.

```EVENT_SIZE``` dan ```EVENT_BUF_LEN``` : Untuk pemantauan perubahan dalam direktori menggunakan inotify.
```cvolatile sig_atomic_t running``` dan ```volatile int security_level``` : Variabel global untuk mengendalikan proses berjalan dan tingkat keamanan yang dapat diubah oleh sinyal.
Fungsi ```rot13``` digunakan untuk mengimplementasikan enkripsi ROT13 untuk mengubah karakter. Ini digunakan untuk mengenkripsi nama berkas dalam logging.

Fungsi ```download_extensions_csv``` :  menggunakan fork untuk menjalankan proses anak yang mendownload file CSV dari Google Drive menggunakan perintah eksternal extensions. Jika proses anak selesai tanpa kesalahan, maka tidak ada masalah. Jika ada kesalahan, pesan kesalahan dicetak.

```c
void log_write(const char *filename, const char *action) {
    FILE *log = fopen("virus.log", "a");


    time_t current_time = time(NULL);
    struct tm *local_time = localtime(&current_time);
    char timestamp[20];
    char *username = getlogin();


    strftime(timestamp, sizeof(timestamp), "%d-%m-%y:%H-%M-%S", local_time);


    const char *file_extension = strrchr(filename, '.');
    int match = 0;


    if (file_extension != NULL) {
        file_extension++;


        FILE *csv_file = fopen("extensions.csv", "r");
        if (csv_file != NULL) {
            char line[MAX_LINE_LENGTH];
            while (fgets(line, sizeof(line), csv_file) != NULL) {
                line[strcspn(line, "\n")] = '\0';


                if (strcmp(file_extension, line) == 0) {
                    match = 1;
                    break;
                }
            }


            fclose(csv_file);
        }
    }
```
Fungsi ```log_write``` digunakan untuk menulis informasi log ke dalam file virus.log berdasarkan nama berkas dan tindakan yang terjadi dan juga memeriksa ekstensi berkas dengan membandingkannya dengan ekstensi yang ada dalam file CSV yang telah diunduh.

```c
if (security_level == 0) {
    // Low mode security
    if (match) {
        fprintf(log, "[%s][%s] - %s - Threat Detected\n", username, timestamp, filename);
    } else {
        fprintf(log, "[%s][%s] - %s - No Threat Detected\n", username, timestamp, filename);
    }
} else if (security_level == 1) {
    // Medium mode security
    if (match) {
        fprintf(log, "[%s][%s] - %s - Moved to Quarantine\n", username, timestamp, filename);
        move_to_quarantine(filename);
    } else {
        fprintf(log, "[%s][%s] - %s - No Threat Detected\n", username, timestamp, filename);
    }
} else if (security_level == 2) {
    // Hard mode security
    if (match) {
        fprintf(log, "[%s][%s] - %s - File Removed\n", username, timestamp, filename);
        delete_file(filename);
    } else {
        fprintf(log, "[%s][%s] - %s - No Threat Detected\n", username, timestamp, filename);
    }
}

fclose(log);
```
* Program melakukan pengecekan ```security_level``` untuk menentukan tingkat keamanan yang aktif saat peristiwa terjadi. Ada tiga tingkat keamanan yang mungkin: 0 (Low), 1 (Medium), atau 2 (Hard).

* Jika ```security_level``` adalah 0 (Low mode security), program akan mencetak pesan ke file log (virus.log) sesuai dengan apakah ada "match" (kecocokan ekstensi berkas dengan ekstensi dalam file CSV yang diunduh):

* Jika ada kecocokan (match), program mencetak pesan bahwa ada ancaman terdeteksi.

* Jika tidak ada kecocokan (match), program mencetak pesan bahwa tidak ada ancaman yang terdeteksi.

* Jika ```security_level``` adalah 1 (Medium mode security), program akan melakukan hal yang sama dengan Low mode security, tetapi jika ada kecocokan, ia juga akan memindahkan berkas ke direktori karantina menggunakan fungsi ```move_to_quarantine```.

* Jika ```security_level``` adalah 2 (Hard mode security), program akan melakukan hal yang sama dengan Medium mode security, tetapi jika ada kecocokan, ia akan menghapus berkas menggunakan fungsi ```delete_file```.

* Terakhir, ditutup file log ```(fclose(log))```.

```c
void monitor_files() {
    int inotify_fd = inotify_init();
    if (inotify_fd == -1) {
        perror("inotify_init failed");
        exit(1);
    }

    int watch_fd = inotify_add_watch(inotify_fd, ".", IN_CREATE | IN_MODIFY);
    if (watch_fd == -1) {
        perror("inotify_add_watch failed");
        exit(1);
    }

    char buffer[EVENT_BUF_LEN];
    while (running) {
        int length = read(inotify_fd, buffer, EVENT_BUF_LEN);
        if (length == -1) {
            perror("read failed");
            exit(1);
        }

        int i = 0;
        while (i < length) {
            struct inotify_event *event = (struct inotify_event *)&buffer[i];

            if (event->len > 0) {
                if (event->mask & IN_CREATE || event->mask & IN_MODIFY) {
                    const char *filename = event->name;
                    log_write(filename, "File changed");
                }
            }

            i += EVENT_SIZE + event->len;
        }
    }

    inotify_rm_watch(inotify_fd, watch_fd);
    close(inotify_fd);
}
```
```inotify_init()``` digunakan untuk inisialisasi inotify dan mengembalikan file descriptor (FD) inotify baru. Jika inisialisasi gagal, fungsi ini akan mengembalikan -1, dan pesan kesalahan akan dicetak dengan perror.

```inotify_add_watch()``` digunakan untuk menambahkan direktori yang akan dipantau oleh inotify. Di sini, kita memantau direktori saat ini (dalam kode ini, direktori di mana program dijalankan) dengan event mask ```IN_CREATE``` dan ```IN_MODIFY```. Jika penambahan watch gagal, pesan kesalahan akan dicetak.

Program memasuki loop while yang terus berjalan selama running adalah benar (yaitu, tidak ada sinyal ```SIGINT``` diterima). Di dalam loop, program menggunakan read untuk membaca peristiwa inotify ke dalam buffer.

Program menggunakan loop untuk mengiterasi melalui semua peristiwa yang telah dibaca dari inotify buffer.

Program memeriksa apakah peristiwa adalah peristiwa pembuatan berkas atau peristiwa modifikasi dengan memeriksa event mask. Jika demikian, program mengekstrak nama berkas dari peristiwa dan memanggil ```log_write``` untuk mencatat peristiwa "File changed" ke dalam file log.

Setelah loop selesai (karena running telah diubah menjadi salah), program membersihkan sumber daya yang digunakan oleh inotify. Ini termasuk menghapus watch descriptor menggunakan ```inotify_rm_watch``` dan menutup file descriptor inotify dengan close.

```c
void handle_signal(int signal) {
    switch (signal) {
        case SIGUSR1:
            printf("Enabling low mode...\n");
            security_level = 0;
            break;
        case SIGUSR2:
            printf("Enabling medium mode...\n");
            security_level = 1;
            break;
        case SIGTERM:
            printf("Enabling hard mode...\n");
            security_level = 2;
            break;
        case SIGINT:
            printf("Shutting Down...\n");
            running = 0;
            break;
        default:
            return;
    }
}

void setup_signal_handlers() {
    signal(SIGUSR1, handle_signal);
    signal(SIGUSR2, handle_signal);
    signal(SIGTERM, handle_signal);
    signal(SIGINT, handle_signal);
}
```
Fungsi ```handle_signal``` adalah fungsi penanganan sinyal. Ini akan dipanggil saat program menerima sinyal tertentu.

Program ini menggunakan switch statement untuk memeriksa jenis sinyal yang diterima.

* Jika program menerima ```SIGUSR1```, ini berarti tingkat keamanan harus diubah menjadi "low" (0). Program mengubah nilai ```security_level``` menjadi 0 dan mencetak pesan bahwa "Low mode" diaktifkan.

* Jika program menerima ```SIGUSR2```, ini berarti tingkat keamanan harus diubah menjadi "medium" (1). Program mengubah nilai ```security_level``` menjadi 1 dan mencetak pesan bahwa "Medium mode" diaktifkan.

* Jika program menerima ```SIGTERM```, ini berarti tingkat keamanan harus diubah menjadi "hard" (2). Program mengubah nilai ```security_level``` menjadi 2 dan mencetak pesan bahwa "Hard mode" diaktifkan.

* Jika program menerima ```SIGINT```, ini berarti program harus dihentikan. Program mengubah nilai running menjadi 0 dan mencetak pesan bahwa program sedang dimatikan.

* Jika program menerima sinyal lain yang tidak dijelaskan dalam switch, maka fungsi ```handle_signal``` akan keluar tanpa melakukan apa pun.

Fungsi ```setup_signal_handlers``` bertugas mengatur penanganan sinyal. Ini memasang handler sinyal ```handle_signal``` untuk sinyal tertentu.

```signal(SIGUSR1, handle_signal)``` mengatur handler untuk ```SIGUSR1```. Jadi, saat program menerima ```SIGUSR1```, handle_signal akan dipanggil. Sama halnya untuk ```SIGUSR2```, ```SIGTERM```, dan ```SIGINT```.

```c
void move_to_quarantine(const char *filename) {
    char quarantine_path[1024] = "quarantine/";
    strcat(quarantine_path, filename);

    // Create the "quarantine" directory if it doesn't exist
    struct stat st = {0};
    if (stat("quarantine", &st) == -1) {
        mkdir("quarantine", 0700);
    }

    // Move the file to the "quarantine" directory
    if (rename(filename, quarantine_path) == -1) {
        perror("Error moving file to quarantine");
    }
}
```
Fungsi ini menerima satu argumen, yaitu ```filename```, yang merupakan nama berkas yang akan dipindahkan ke direktori karantina.

* Pertama, fungsi membuat variabel ```quarantine_path``` yang digunakan untuk menentukan jalur lengkap ke direktori karantina. Jalur ini terdiri dari string "quarantine/" diikuti oleh nama berkas ```filename```. Ukuran ```quarantine_path``` diatur ke 1024 byte.

* Selanjutnya, kode memeriksa apakah direktori "quarantine" sudah ada atau belum. Ini dilakukan dengan menggunakan stat. Jika direktori tidak ada, maka kode akan membuat direktori tersebut dengan hak akses 0700 (hanya pemilik direktori yang dapat membaca, menulis, dan menjalankannya).

* Terakhir, kode mencoba untuk memindahkan berkas dengan menggunakan rename dari jalur asal (```filename```) ke jalur karantina (```quarantine_path```). Jika pemindahan gagal, kode akan mencetak pesan kesalahan menggunakan perror.

```c
void delete_file(const char *filename) {
    if (remove(filename) == -1) {
        perror("Error deleting file");
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s [-p low|medium|hard]\n", argv[0]);
        return 1;
    }


    if (strcmp(argv[1], "-p") != 0) {
        fprintf(stderr, "Usage: %s [-p low|medium|hard]\n", argv[0]);
        return 1;
    }
   
    download_extensions_csv();
   
    setup_signal_handlers();
    monitor_files();

    return 0;
}
```
```filename``` merupakan nama berkas yang akan dihapus.

Fungsi menggunakan remove untuk mencoba menghapus berkas dengan nama yang diberikan. Jika operasi penghapusan gagal (misalnya, berkas tidak ada), fungsi akan mencetak pesan kesalahan dengan perror.

Fungsi main adalah titik masuk utama program.

* Pertama, kode memeriksa jumlah argumen baris perintah (```argc```). Jika jumlah argumen bukan 2 (yaitu, tidak ada argumen atau argumen yang salah), program mencetak pesan kesalahan dan mengembalikan status keluar 1 untuk menunjukkan kesalahan.

* Selanjutnya, kode memeriksa apakah argumen pertama (```argv[1]```) adalah string "-p". Jika tidak, program mencetak pesan kesalahan yang sama dan mengembalikan status keluar 1.

Jika kedua pemeriksaan di atas berhasil, program menjalankan serangkaian tindakan:

* Memanggil ```download_extensions_csv``` untuk mengunduh file CSV dengan ekstensi yang digunakan dalam pemantauan.

* Memanggil ```setup_signal_handlers``` untuk mengatur penanganan sinyal.

* Memanggil ```monitor_files``` untuk memulai pemantauan perubahan berkas dan peristiwa yang sesuai.

* Setelah itu, program mengembalikan status ```return 0```, yang menunjukkan bahwa program telah selesai berjalan dengan sukses.

**Cara nge run di terminal:
gcc antivirus.c -o antivirus
Jalankan program dengan argumen yang sesuai: ./antivirus -p low atau ./antivirus -p medium atau ./antivirus -p hard. Ini akan mengatur tingkat keamanan sesuai dengan command yang diberikan.**

# Revisi
Dalam pengerjaan modul 2 ini poin-poin yang membutuhkan revisi adalah sebagai berikut :

* Soal 3
    * Program killer ketika kode dijalankan dengan MODE_B atau argumen (hanya kill parent process dan membiarkan child process hingga selesai) gagal berjalan.
* Soal 4
    * Program tidak dapat dijalankan di terminal (masiha ada error yang belum terselesaikan).

Semua hal diatas sudah terselesaikan dan dimasukkan dalam source code serta didokumentasikan pada bagian [Penjelasan](#penjelasan)