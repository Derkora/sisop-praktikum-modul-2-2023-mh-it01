#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>

//mencari sus jika ketemu 1 jika ga 0
int MR_SUS(const char *filedottxt){
    FILE *fileeee = fopen(filedottxt, "r");
    if(!fileeee) return 0;

    char barise[200];
    while(fgets(barise, sizeof(barise), fileeee))
        if(strstr(barise, "SUSPICIOUS")){
            fclose(fileeee);
            return 1;
        }
    
    fclose(fileeee);
    return 0;
}

//membuat log berdasarkan waktu realtim
void buatdotlog(const char *kecleaner, const char *file){
    time_t waktu = time(NULL);
    struct tm *date = localtime(&waktu);
    char stamplog[30];
    strftime(stamplog, 30, "%Y-%m-%d %H:%M:%S", date);

    FILE *lognya = fopen("/home/steven/cleaner.log", "a");
    if(lognya){
        fprintf(lognya, "[%s] '/%s/%s' has been removed.\n", stamplog, kecleaner, file);
        fclose(lognya);
    }
}

//proses cleanernya
void cleanerdotC(const char *kecleaner) {
    struct dirent *entry;
    struct stat stat;

    DIR *clean = opendir(kecleaner);
    if (!clean) return;

    chdir(kecleaner);

    while((entry = readdir(clean))){
        if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;

        if(lstat(entry->d_name, &stat) == -1) continue;

        if(S_ISDIR(stat.st_mode)) cleanerdotC(entry->d_name);
        else if(S_ISREG(stat.st_mode) && MR_SUS(entry->d_name)){ //jika 1 ya ngapus, 0 keluar
            remove(entry->d_name);
            buatdotlog(kecleaner, entry->d_name);
        }
        sleep(30);
    }

    closedir(clean);
}

int main(int argc, char *argv[]){
    /*
    (gcc cleaner.c -o cleaner)
    ./cleaner home/user/cleaner
    untuk menjalankan program
    */
    pid_t pid, sid;
    pid = fork();
    if(pid < 0){
        exit(EXIT_FAILURE);
    }

    if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    umask(0);
    sid = setsid();
    if(sid < 0){
        exit(EXIT_FAILURE);
    }

    if(chdir("/") < 0){
        exit(EXIT_FAILURE);
    } 

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while(1){
        cleanerdotC(argv[1]);
    }

    return 0;
}