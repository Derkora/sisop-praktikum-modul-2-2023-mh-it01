#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>

int main() {
    /*
    (gcc cavs.c -o cavs; ./cavs)
    untuk menjalankan program
    */
    pid_t make;
    int status;
    
    //a untuk membuat direktori
    make = fork();
    if(make == 0){
        char *bashdotsh[] = {"mkdir", "players", NULL};
        execv("/bin/mkdir", bashdotsh);
    }

    //b mengunduh, unzip, dan remove
    pid_t ngunduh, nuzip, mr_zip; 
    ngunduh = fork(); //unduh
    if(ngunduh == 0){
        char *bashdotsh[] = {"wget", "--no-check-certificate", "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "-O", "pemainbb", NULL};
        execv("/usr/bin/wget", bashdotsh);
    }
    waitpid(ngunduh, &status, 0);

    nuzip = fork(); //unzip
    if(nuzip == 0){
        char *bashdotsh[] = {"unzip", "pemainbb", "-d", "players", NULL};
        execv("/usr/bin/unzip", bashdotsh);
    }
    waitpid(nuzip, &status, 0);

    mr_zip = fork(); //remove
    if(mr_zip == 0){
        char *bashdotsh[] = {"rm", "pemainbb", NULL};
        execv("/bin/rm", bashdotsh);
    }
    //c menghilangkan pemain selain cavs
    pid_t mr_non_cavs; 
    mr_non_cavs = fork();
    if(mr_non_cavs == 0){
        char *bashdotsh[] = {"find", "players", "-type", "f", "!", "-name", "*Cavaliers*.png", "-delete", NULL};
        execv("/bin/find", bashdotsh);
    }
    //d segregasi pemain
    //pembagian wilayah
    pid_t buat_dirPG, buat_dirSG, buat_dirSF, buat_dirPF, buat_dirC;
    buat_dirPG = fork();
    if (buat_dirPG == 0){
        char *bashdotsh[] = {"mkdir", "-p", "players/point_guard", NULL};
        execv("/bin/mkdir", bashdotsh);
    }

    buat_dirSG = fork();
    if (buat_dirSG == 0){
        char *bashdotsh[] = {"mkdir", "-p", "players/shooting_guard", NULL};
        execv("/bin/mkdir", bashdotsh);
    }

    buat_dirSF = fork();
    if (buat_dirSF == 0){
        char *bashdotsh[] = {"mkdir", "-p", "players/small_forward", NULL};
        execv("/bin/mkdir", bashdotsh);
    }

    buat_dirPF = fork();
    if(buat_dirPF == 0){
        char *bashdotsh[] = {"mkdir", "-p", "players/power_forward", NULL};
        execv("/bin/mkdir", bashdotsh);
    }

    buat_dirC = fork();
    if(buat_dirC == 0){
        char *bashdotsh[] = {"mkdir", "-p", "players/center", NULL};
        execv("/bin/mkdir", bashdotsh);
    }

    //proses segregasi
    pid_t mindah_dirPG, mindah_dirSG, mindah_dirSF, mindah_dirPF, mindah_dirC;
    mindah_dirPG = fork();
    if(mindah_dirPG == 0){
        char *bashdotsh[] = {"find", "players", "-type", "f", "-iname", "*-PG-*", "-exec", "mv", "{}", "players/point_guard", ";", NULL};
        execv("/bin/find", bashdotsh);
    }
    while(wait(&status) > 0);

    mindah_dirSG = fork();
    if(mindah_dirSG == 0){
        char *bashdotsh[] = {"find", "players", "-type", "f", "-iname", "*-SG-*", "-exec", "mv", "{}", "players/shooting_guard", ";", NULL};
        execv("/bin/find", bashdotsh);
    }
    while(wait(&status) > 0);

    mindah_dirSF = fork();
    if(mindah_dirSF == 0){
        char *bashdotsh[] = {"find", "players", "-type", "f", "-iname", "*-SF-*", "-exec", "mv", "{}", "players/small_forward", ";", NULL};
        execv("/bin/find", bashdotsh);
    }
    while(wait(&status) > 0);

    mindah_dirPF = fork();
    if(mindah_dirPF == 0){
        char *bashdotsh[] = {"find", "players", "-type", "f", "-iname", "*-PF-*", "-exec", "mv", "{}", "players/power_forward", ";", NULL};
        execv("/bin/find", bashdotsh);
    }
    while(wait(&status) > 0);

    mindah_dirC = fork();
    if(mindah_dirC == 0){
        char *bashdotsh[] = {"find", "players", "-type", "f", "-iname", "*-C-*", "-exec", "mv", "{}", "players/center", ";", NULL};
        execv("/bin/find", bashdotsh);
    }
    while(wait(&status) > 0);

    //e Formasi.txt emot skull
    char *folder[] = {"point_guard", "shooting_guard", "small_forward", "power_forward", "center"};
    char shorten[][10] = {"PG", "SG", "SF", "PF", "C"};
    int count[5] = {0};

    for(int i = 0; i < 5; i++){
        char path[100];
        sprintf(path, "players/%s", folder[i]);
        struct dirent *entry;
        DIR *players = opendir(path);
        if(players != NULL){
            while((entry = readdir(players)) != NULL){
                if(entry->d_type == DT_REG) count[i]++;
            }
            closedir(players);
        }
    }
    
    //ngitung dari filenya
    FILE *formasi = fopen("Formasi.txt", "w"); //buat baru
    if(formasi != NULL){
        for (int i = 0; i < 5; i++){
            fprintf(formasi, "%s: %d\n", shorten[i], count[i]);
        }
        fclose(formasi);
    }   

    //f foto lagi!!!!!!!!!!!!!!!!!!!!!
    pid_t buat_dirclutch, cp_pemain, cp_theblock;
    buat_dirclutch = fork();
    if(buat_dirclutch == 0){
        char *bashdotsh[] = {"mkdir", "clutch",NULL};
        execv("/bin/mkdir", bashdotsh);
    }

    //salin foto
    cp_pemain = fork();
    if(cp_pemain == 0){
        char *bashdotsh[] = {"find", "players", "-type", "f", "-iname", "*Kyrie*", "-exec", "cp", "{}", "clutch", ";", NULL};
        execv("/bin/find", bashdotsh);
    }
    while(wait(&status) > 0 );

    cp_theblock = fork();
    if(cp_theblock == 0){
        char *bashdotsh[] = {"find", "players", "-type", "f", "-iname", "*LeBron*", "-exec", "cp", "{}", "clutch", ";", NULL};
        execv("/bin/find", bashdotsh);
    }
    while(wait(&status) > 0 );

    return 0;
    
}