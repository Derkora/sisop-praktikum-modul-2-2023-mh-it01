#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <signal.h>

//albudi a.k.a. calx si sepuh pelukis whwwwwwehwehwhehhwewh

int main(int argc, char** argv) {
    if (argc == 2) {
        FILE* kill_program = fopen("kill.sh", "w");
        fprintf(kill_program, "#!/bin/bash\n");
        if (!strcmp("-a", argv[1])) {
            fprintf(kill_program, "killall -9 lukisan\nrm $0\n");
        } else if (!strcmp("-b", argv[1])) {
            fprintf(kill_program, "kill_parent()\n");
                fprintf(kill_program, "kill ${@: -1}\n");
                fprintf(kill_program, "}\n");
                fprintf(kill_program, "kill_parent $(pidof lukisan)\nrm $0\n");
        }

        fclose(kill_program);

        pid_t chi = fork();
        if (chi < 0){
            exit(EXIT_FAILURE);
        }
        if (chi == 0){
            execlp("bin/chmod", "chmod", "+x", "kill.sh", NULL);
            exit(0);
        }

        pid_t par, sid;

        par = fork();

        if (par < 0) {
            exit(EXIT_FAILURE);
        }
        
        if (par > 0) {
            exit(EXIT_SUCCESS);
        }

        umask(0);

        sid = setsid();
        if (sid < 0) {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
    }

    while (1) {
        pid_t make_folder;
        time_t current = time(NULL);
        time(&current);
        struct tm* time_info;
        time_info = localtime(&current);
        char timeformat[60];
        strftime(timeformat, sizeof(timeformat), "%Y-%m-%d_%H:%M:%S", time_info);
        //printf("%s", timeformat);

        make_folder = fork();

        if (make_folder == 0) {
            execlp("/bin/mkdir", "mkdir", timeformat, NULL);
            exit(0);
        } else {
            pid_t loop_download = fork();
            if (loop_download == 0) {
                for (int i = 0; i < 15; i++) {
                    //donlod gambarnya
                    pid_t download_pic = fork();

                    if (download_pic == 0) {
                        current = time(NULL);
                        char link[100];
                        sprintf(link, "https://source.unsplash.com/%ldx%ld", (current%1000)+50, (current%1000)+50);
                        //printf("%s", link);

                        time_t time_pic;
                        struct tm *time_now_pic;
                        char timeformat_pic[30];
                        char download_dir[100];

                        time(&time_pic);
                        time_now_pic = localtime(&time_pic);
                        strftime(timeformat_pic, sizeof(timeformat_pic), "%Y-%m-%d_%H:%M:%S", time_now_pic);
                        sprintf(download_dir, "%s/%s.jpeg", timeformat, timeformat_pic);

                        char *arg[] = {"wget", "-q", "-O", download_dir, link, NULL};
                        execv("/bin/wget", arg);
                    }
                    sleep(5);
                }
                int status;
                wait(&status);
                char zname[100];
                sprintf(zname, "%s.zip", timeformat);
                pid_t zip_program = fork();

                if (zip_program == 0) {
                    char *arg[] = {"zip", zname, "-rm", timeformat, NULL};
                    execv("/bin/zip", arg);
                }
                exit(0);
            }
        }
        sleep(30);
    }
}