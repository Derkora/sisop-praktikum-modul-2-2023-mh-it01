#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/inotify.h>

//soal 4.a
int main() {
    pid_t unduh, enkripsi;


    unduh = fork();
    if(unduh == 0){
        char *argv[] = {"wget", "--no-check-certificate", "https://drive.google.com/uc?export=download&id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5/", "-O", "extensions", NULL};
        execv("/usr/bin/wget", argv);
    }
    waitpid(unduh, &status, 0);
    enkripsi = fork();
    return 0;
}

//soal 4.b

int pendeteksiVirus(const char *str) {
    
    const char *virus = "contoh_virus";

    return strstr(str, virus) != NULL;
}

int main() {
    FILE *file;
    char buffer[1024];

    
    file = fopen("extensions.csv", "r");

    if (file == NULL) {
        perror("Tidak bisa membuka file");
        return 1;
    }

    // membaca file
    while (fgets(buffer, sizeof(buffer), file) != NULL) {
        if (pendeteksiVirus(buffer)) {
            printf("Virus terdeteksi!\n");
            fclose(file);
            return 1;
        }
    }

    printf("File bebas virus.\n");
    fclose(file);
    return 0;
}



// soal 4.c
void checkFolder(const char *folderPath) {
    DIR *dir;
    struct dirent *entry;

    if ((dir = opendir(folderPath)) == NULL) {
        perror("opendir");
        exit(EXIT_FAILURE);
    }

    printf("sisop_infected'%s':\n", folderPath);
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            printf("%s\n", entry->d_name);
        }
    }

    closedir(dir);
}

// untuk nge stop program
void stopProgram(int signum) {
    printf("Received SIGTERM. Exiting...\n");
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s <folder_path>\n", argv[0]);
        return 1;
    }

    const char *folderPath = argv[1];

    // Ngecek Directory
    struct stat pathStat;
    if (stat(folderPath, &pathStat) == -1 || !S_ISDIR(pathStat.st_mode)) {
        perror("Invalid directory");
        return 1;
    }

    
    signal(SIGTERM, stopProgram);

    // Fork untuk membuat proses daemon
    pid_t pid = fork();

    if (pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        // Proses induk keluar
        exit(EXIT_SUCCESS);
    }

    // nge run daemon
    umask(0);
    setsid();

    
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    // nge Loop utama
    while (1) {
        checkFolder(folderPath);
        sleep(1); // nunggu 1 detik buat di cek lagi 
    }
    return: 0;

}

//4.d

void low() {
    printf("Running low mode..\n")
}

void medium() {
    printf("Running medium mode..\n")
}

void hard() {
    printf("Running hard mode..\n")
}

int main (int argc, char *argv[]) {
    if (argc != 3) {
        printf("Unidentified Command!\n")
        return 0;
    }

    if (strcmp(argv[1], "-p") != 0) {
        printf("Unidentified Command Prompt!\n");
        return 0;
    }

    if (strcmp(argv[1], "low") == 0) {
        printf("low mode activated!\n");
        
    } else if (strcmp(argv[1], "medium") == 0) {
        printf("medium mode activated!\n");
        
    } else if (strcmp(argv[1], "hard") == 0) {
        printf("hard mode activated!\n");
        
    } else {
        printf("Unidentified mode: %s\n", argv[1]);
        return 1;
}

//4.e

else if (strcmp(argv[1], "shutdown") == 0) {
        printf("Shutting down...\n");
}

return 0;
}
